//processamento dos dados do formulario "index.html"

//acessando as eleventos formulario do html
const formulario = document.getElementById("formulario1");

//adiciona um ouvinte de eventos a um elemento HTML
formulario.addEventListener("submit", function (evento) {
  evento.preventDefault(); /*previne o compartilhamento padrão de um elemento*/
  /*HTML em resposta a um evento*/

  //constante para tratar os dados recebidos dos elementos do formulario
  const nome = document.getElementById("nome").value;
  const email = document.getElementById("email").value;

  //exibe um alerta com os dados coletados
 // alert(`nome: ${nome} --- E-mail: ${email}`);

  const updateresultado = document.getElementById("resultado");

  updateresultado.value = `nome: ${nome}e-mail: ${email}`;

  updateresultado.style.width = updateresultado.scrollWidth + "px";

});
