const {createApp} = Vue;
createApp({
    data(){
        return {
            randomIndex: 0,
            randomIndexInternet: 0,

            //vetor de imagens locais 
            imagensLocais:[
                './imagens/lua.jpg',
                './imagens/SENAI_logo.png',
                './imagens/sol.jpg'
            ],

                imagensLocais:[
                    'https://img.freepik.com/vetores-gratis/bela-casa_24877-50819.jpg',
                    'https://images.tcdn.com.br/img/img_prod/834271/bicicleta_passeio_aro_26_potenza_barra_forte_freio_vbreak_preto_2619_1_20349c9d968884aa876cdbf07c0fb185.jpg',
                    'https://www.designi.com.br/images/preview/10807451.jpg',
                ]


        };//fim return
    },//fim data

    computed:{
        randomImage()
        {
            return this.imagensLocais[this.randomIndex];
        },// fim randomImage

        randomImageInternet()
        {
            return this.imagensInternet[this.randomIndexInternet];
        }// fim randomInternet

    },//fim computed

    methods:{
        getRandomImage()
        {
            this.randomIndex=Math.floor(Math.random()*this.imagensLocais.length);

            this.randomIndexInternet = Math.floor(Math.random( )*this.imagensInternet.length);

        }
    },//fim methods
}).mount("#app");